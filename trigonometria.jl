#MAC0110 - MiniEP7
#Alxelio Esteves - 11796980
										#git commit -s -m "Add a header to the file"


#Add MiniEP7 Sine, Cosine and Tangent Functions

#Auxiliary functions were needed/requested

										#git add -A (for all)
										#git commit -s -m "Add MiniEP7 Sine, Cosine and Tangent Functions"
										#git push origin parte1


#Função Exponenciação
function exponenciacao(n)::Float64

	d = (2 ^ (2 * n)) * ((2 ^ (2 * n))-1)
	return(d)

end




#Função Exponenciação 2
function exponenciacao2(n, x::Float64)
	e = (x ^ (2*n-1))
	return(e)

end




#Função de Bernoulli
function bernoulli(n)
	n *= 2
	A = Vector{Rational{<:Real}}(undef, n+1)

	for m = 0 : n
		A[m+1] = 1 // (m + 1)
		for j = m : -1 : 1
			A[j] = j * (A[j] - A[j+1])
		end
	end

	return abs(A[1])

end




#Função Fatorial
function fatorial(k)
	m=1

	for i in 1:k
		m = m * i
	end

	return(m)

end




#Através das derivadas primeira, derivada segunda, derivada terceira, derivada n-ésima da função, 
#para um valor bem próximo de 0 (zero) obtemos a fórmula da Série de Taylor para o Cosseno
#Função Cosseno
function cos(x)
	#Converte graus em radianos, pois a Série de Taylor exige radianos
	#rad = (pi * x) / 180
	c=0

	for n in 0:10  #As melhores iterações estão em 10, 8 e 9
		c = c + (((-1) ^ n) * (x ^ (2 * n))) / fatorial(2*n) 
	end

	return(c)

end
	



#Através das derivadas primeira, derivada segunda, derivada terceira, derivada n-ésima da função, 
#para um valor bem próximo de 0 (zero) obtemos a fórmula da Série de Taylor para o Seno
#Função Seno
function sin(x)
	#Converter graus em radianos, pois a série de Taylor exige radianos
	#rad = (pi * x) / 180
	c=0

	for n in 0:9  #As melhores iterações estão em 9, 8 e 6 
		c = c + (((-1) ^ n) * (x ^ (2n+1))) / fatorial(2*n+1)
	end

	return(c)

end




#Através das derivadas primeira, derivada segunda, derivada terceira, derivada n-ésima da função, 
#para um valor bem próximo de 0 (zero) obtemos a fórmula da Série de Taylor para a Tangente
#Função Tangente
function tan(x::Float64)
	
	c=0

	for n in 1:10  #A melhor iteração está em 10, 
		c = c + ((exponenciacao(n) * ((bernoulli(n)) * exponenciacao2(n, x)))  / (fatorial(2 * n)))
	end
	
	return(c)

end


#################################### Parte 2 ##################################################

#Nas funções check_ foram utilizadas uma margem de erro em que consiste
#que o valor de erro aceitável esteja entre 0 e 1.0 - Feito na unha mesmo.
#Sem a utilização da função \approx, pois foi a que melhor se comportou. 


										#git add -A (for all)
										#git commit -s -m "Add MiniEP7 Check Functions part 2.1 exercise"
										#git push origin parte2


#Função check_sin para verificar os valores de seno
function check_sin(value, x)
							# ≈ \approx TAB 
	
	k = abs(big(sin(x)))
							#println(k)
							#println(value)

	res = abs(big(k-value))
							#println(res)

	if res <= 1.0
		return true
	else
		return false
	end

end




#Função check_cos para verificar os valores de cosseno
function check_cos(value, x)
							# ≈ \approx TAB 
	
	k = abs(big(cos(x)))
							#println(k)
							#println(value)

	res = abs(big(k-value))
							#println(res)

	if res <= 1.0
		return true
	else 
		return false
	end

end




#Função check_tan para verificar os valores da tangente
function check_tan(value, x)
							# ≈ \approx TAB 
	
	k = abs(big(tan(x)))
							#println(k)
							#println(value)

	res = abs(big(k-value))
							#println(res)

	if res <= 1.0
		return true
	else
		return false
	end

end




########################### Parte 3 ##############################################



using Test

#Funções Testes
function test()
	@test sing(0) == 0.0

	@test cosg(0) == 1.0 

	@test tang(0) == 0

	@test sing(pi/6) ≈ 0.5

	@test cosg(pi/6) == (sqrt(3)/2)

	@test tang(pi/6) ≈ (sqrt(3)/3)

	@test sing(pi/4) ≈ (sqrt(2)/2)

	@test cosg(pi/4) ≈ (sqrt(2)/2) 

	@test tang(pi/4) ≈ 1.0



#Mesmo utilizando a função de aproximação, 
#o código apresenta falha nos testes apesar
#de ser avaliado como true.



	@test check_sin(0.866025, pi/3) ≈ (sqrt(3)/2) 

	@test check_cos(0.500000, pi/3) ≈ 0.5

	@test check_tan(1.732050, pi/3) ≈ (sqrt(3))

	@test check_sin(1.00000, pi/2) ≈ 1.0 

	@test check_cos(0.0000, pi/2) ≈ 0.0

	@test check_tan(Inf, pi/2) ≈ Inf


	println("Final dos Testes")

end

test()



################################### Parte 4 #############################################


										#git add -A (for all)
										#git commit -s -m "Rename MiniEP7 Functions"
										#git push origin master

#As alterações aqui foram feitas simultaneamente com outras funções de testes automáticos na branch parte3
#Antes de serem atribuídas remotamente



#Função Cosseno
function taylor_cos(x)
	#Converte graus em radianos, pois a Série de Taylor exige radianos
	#rad = (pi * x) / 180
	c=0

	for n in 0:10  #As melhores iterações estão em 10, 8 e 9
		c = c + (((-1) ^ n) * (x ^ (2 * n))) / fatorial(2*n) 
	end

	return(c)

end
	



#Função Seno
function taylor_sin(x)
	#Converter graus em radianos, pois a série de Taylor exige radianos
	#rad = (pi * x) / 180
	c=0

	for n in 0:9  #As melhores iterações estão em 9, 8 e 6 
		c = c + (((-1) ^ n) * (x ^ (2n+1))) / fatorial(2*n+1)
	end

	return(c)

end




#para um valor bem próximo de 0 (zero) obtemos a fórmula da Série de Taylor para a Tangente
#Função Tangente
function taylor_tan(x::Float64)
	
	c=0

	for n in 1:10  #A melhor iteração está em 10, 
		c = c + ((exponenciacao(n) * ((bernoulli(n)) * exponenciacao2(n, x)))  / (fatorial(2 * n)))
	end
	
	return(c)

end




























